package media.radar.sample;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import media.radar.radarkit.RadarKit;
import media.radar.radarkit.data.RKAction;
import media.radar.radarkit.data.RKError;
import media.radar.radarkit.data.RKGeofence;
import media.radar.radarkit.data.RadarDataTrigger;


public class MainActivity extends AppCompatActivity implements RadarKit.interactionListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static boolean onForeground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        onForeground = true;

        RadarKit.instance().delegate(this);
        RadarKit.instance().setMinimumErrorLevel(RKError.Level.ALL);
        RadarKit.instance().start("_YOUR_APP_KEY_");

    }

    //------------------------------------------------------------------------------
    public void onUpdateStarted() {
        Log.i(TAG, "------------->onUpdateStarted!");
    }
    //------------------------------------------------------------------------------

    //------------------------------------------------------------------------------
    public void onUpdateFinished() {
        Log.i(TAG, "------------->onUpdateFinished!");

        String radarVersion = RadarKit.instance().getRadarVersion();
        String deviceIdentifier = RadarKit.instance().getDeviceIdentifier();
        Date radarLastUpdate = RadarKit.instance().getRadarLastUpdate();

        Log.i(TAG, "RadarVersion = " + radarVersion);
        Log.i(TAG, "DeviceIdentifier = " + deviceIdentifier);
        Log.i(TAG, "RadarLastUpdate = " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(radarLastUpdate));

        Bitmap homeImage = RadarKit.instance().homeScreenImage();
        String homeUrl = RadarKit.instance().homeScreenUrl();

        Log.i(TAG, "HomeUrl = " + homeUrl);

    }
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    public void onProgressUpdated(final String progress) {

        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "Progress = " + progress);
                }
            });
        } catch (final Exception ex) {}

    }
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    public void onError(RKError error) {
        Log.i(TAG, "--------------onError-----------");
        Log.i(TAG, "Code = " + error.getCode());
        Log.i(TAG, "Type = " + error.getType());
        Log.i(TAG, "Level = " + error.getLevel());
        Log.i(TAG, "Description = " + error.getDescription());
        Log.i(TAG, "-----------------------------");


        if (error.getType().equals(RKError.Type.INITIALIZE_OK)) {
            Log.i(TAG, "Radar.Media.Kit Initialized Ok!");
        }

        if (error.getType().equals(RKError.Type.INITIALIZE_ERROR)) {
            Log.i(TAG, "Radar.Media.Kit cannot initialize.");
        }

        if (error.getType().equals(RKError.Type.LOCATION_PERMISSION_NOT_GRANTED)) {
            Log.i(TAG, "Radar.Media.Kit needs Location permission granted.");
        }
    }
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    @Override
    public void onLocationUpdated(List<RadarDataTrigger> triggers, Location location) {
        Log.i(TAG, "------------->onLocationUpdated!");

        //------------device geo position-------------
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        double accuracy = location.getAccuracy();

        Log.i(TAG, "Position: "+ latitude + "," + longitude);

        //---------------triggers---------------------
        for (RadarDataTrigger item : triggers) {

            String triggerName = item.name;
            Boolean triggerStatus = item.status;

            switch (item.type) {
                case beacon:
                    Log.i(TAG, "Beacon: " + triggerName + " = " + triggerStatus);
                    break;

                case geofence:
                    if(item.geofenceType == RKGeofence.Type.circular){
                        double radius = item.radius;
                        List<LatLng> coordinates = item.coordinates;
                        Log.i(TAG, "Geofence Circular: " + triggerName + " = " + radius + "mts");
                    }

                    if(item.geofenceType == RKGeofence.Type.polygonal){
                        List<LatLng> coordinates = item.coordinates;
                        Log.i(TAG, "Geofence Polygonal: " + triggerName);
                    }
                    break;
            }

        }

    }
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    @Override
    public void onBeaconUpdated(List<RadarDataTrigger> beacons) {
        Log.i(TAG, "------------->onBeaconUpdated!");
    }
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    @Override
    public void onGeofenceUpdated(List<RadarDataTrigger> geofences) {
        Log.i(TAG, "------------->onGeofenceUpdated!");
    }
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    @Override
    public void shouldProcess(RKAction action){
        Log.i(TAG, "------------->shouldProcess!");

        //--------------------metadata------------------------
        if(action.type == RKAction.Type.METADATA) {
            Map<String, String> textAttributes = action.metadata.textAttributes;
            if (!textAttributes.isEmpty()) {
                Log.i(TAG, "Text Attributes:");
                for (Map.Entry<String, String> item : textAttributes.entrySet()) {
                    Log.i(TAG, item.getKey() + " = " + item.getValue());
                    if(item.getKey().equals("change_background_color")){
                        try {
                            View view = this.getWindow().getDecorView();
                            view.setBackgroundColor(Color.parseColor(item.getValue()));
                        }catch(Exception e){}
                    }
                }
            }
        }
        //----------------------------------------------------


        //--------------------template------------------------
        if(action.type == RKAction.Type.TEMPLATE) {
            String title = action.notificationTitle;
            String description = action.notificationText;

            Log.i(TAG, "Notification: " + title + "\n" + description);

            if(onForeground){
                sendToScreen(action);
            }else{
                sendNotification(action);
            }
        }
        //----------------------------------------------------

    }
    //------------------------------------------------------------------------------

    //------------------------------------------------------------------------------
    public void onWindowFocusChanged(boolean hasFocus){
        try {
            if(hasFocus) {
                onForeground = true;
            }else{
                onForeground = false;
            }
        }
        catch(Exception ex){}
    }
    //------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------
    private void sendNotification(RKAction action) {

        String title = action.notificationTitle;
        String description = action.notificationText;
        String iconBitmap = action.notificationIcon;

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,"MyChannelId_01")
                .setSmallIcon(R.drawable.status_bar_icon)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.icon))
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(description))
                .setContentText(description)
                .setAutoCancel(true)
                ;
        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

        if(!iconBitmap.isEmpty()){
            Resources res = this.getResources();
            int height = (int) res.getDimension(android.R.dimen.notification_large_icon_height);
            int width = (int) res.getDimension(android.R.dimen.notification_large_icon_width);

            byte[] decodedByteArray = Base64.decode(iconBitmap, Base64.NO_WRAP);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedByteArray, 0, decodedByteArray.length);
            Bitmap scaledIcon = Bitmap.createScaledBitmap(decodedBitmap, width, height, false);

            if(height>0 && width>0){
                mBuilder.setLargeIcon(scaledIcon);
            }else{
                mBuilder.setLargeIcon(decodedBitmap);
            }
        }

        Intent intent = new Intent(this, MessageViewActivity.class);
        Random r = new Random();
        int contentId = r.nextInt(1000 - 1) + 1;

        intent.putExtra("title",  action.notificationTitle);
        intent.putExtra("data",  action.template.content);
        intent.putExtra("path",  action.template.contentRootPath);
        intent.putExtra("color",  action.template.backgroundColor);

        PendingIntent pIntent = PendingIntent.getActivity(this, contentId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pIntent);
        mNotificationManager.notify(contentId, mBuilder.build());
    }
    //-------------------------------------------------------------------------------------------------------------------------


    //-------------------------------------------------------------------------------------------------------------------------
    private void sendToScreen(RKAction action){

        Intent intent = new Intent(this, MessageViewActivity.class);

        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra("title",  action.notificationTitle);
        intent.putExtra("data",  action.template.content);
        intent.putExtra("path",  action.template.contentRootPath);
        intent.putExtra("color",  action.template.backgroundColor);

        startActivity(intent);
    }
    //-------------------------------------------------------------------------------------------------------------------------

}
