package media.radar.sample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageViewActivity  extends Activity {

    private WebView mWebView;

    //-------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_message_view);

        mWebView = (WebView) findViewById(R.id.content_webview);

        //----------------------------
        ImageView btnClose = (ImageView) findViewById(R.id.btnContentClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                close();
            }
        });
        //----------------------------

        try {
            Intent intent = getIntent();
            String title = intent.getStringExtra("title");
            String data = intent.getStringExtra("data");
            String path = intent.getStringExtra("path");
            String color = intent.getStringExtra("color");

            TextView message_title = (TextView) findViewById(R.id.message_title);
            message_title.setText(title);
            showTemplate(data, path);
        } catch (Exception e) {}

    }
    //-------------------------------------------------------------------

    //-------------------------------------------------------------------
    public void close(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    //-------------------------------------------------------------------

    //-------------------------------------------------------------------
    public void showTemplate(String data, String path){
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        String imagePath = "file://"+ path;
        mWebView.loadDataWithBaseURL(imagePath, data, "text/html", "utf-8",null);
        mWebView.setWebViewClient(new WebViewClient());
    }
    //-------------------------------------------------------------------

}
